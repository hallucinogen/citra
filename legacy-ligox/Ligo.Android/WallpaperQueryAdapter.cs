using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.Content;
using Android.Views;
using Android.Widget;
using Android.Graphics.Drawables;
using Parse;

namespace Ligo
{
	public class WallpaperQueryAdapter : ParseQueryAdapter<Wallpaper>
	{
    public WallpaperQueryAdapter(Context context) : base(context, new CreateQueryDelegate(WallpaperQueryAdapter.createWallpaperQuery))
		{
			this.ObjectsPerPage = 10;
		}

		protected override View GetItemView(Wallpaper obj, View v, ViewGroup parent)
		{
			if (v == null)
			{
				v = View.Inflate(this.Context, Resource.Layout.TimelineItem, null);
			}

			TextView titleView = (TextView)v.FindViewById(Resource.Id.title);
			ParseImageView imageView = (ParseImageView)v.FindViewById(Resource.Id.image);

			titleView.Text = obj.Title;
			if ((imageView.Drawable as BitmapDrawable) != null)
			{
				(imageView.Drawable as BitmapDrawable).Bitmap.Recycle();
			}

      imageView.SetParseFile(obj.Image.ImageFile);
      imageView.LoadInBackground();

			return v;
		}

    internal static ParseQuery<Wallpaper> createWallpaperQuery() {
      var query = new ParseQuery<Wallpaper>();
      query.Include("image");

      return query;
    }
	}
}

