using System;
using Android.Views;
using Android.OS;
using Android.Support.V4.App;
using Android.Widget;
using Parse;

namespace Ligo
{
	public class PopularFragment : Fragment
	{
		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			View rootView = inflater.Inflate(Resource.Layout.Timeline, container, false);

      ListView listView = (ListView)rootView.FindViewById(Resource.Id.list_view);
      var adapter = new WallpaperQueryAdapter(this.Activity);
      listView.Adapter = adapter;

			return rootView;
		}
	}
}

