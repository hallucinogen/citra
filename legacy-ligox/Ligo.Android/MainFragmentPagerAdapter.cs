using System;
using Android.Content;
using Android.Support.V4.App;

namespace Ligo
{
	public class MainFragmentPagerAdapter : FragmentStatePagerAdapter
	{
		public MainFragmentPagerAdapter(FragmentManager manager) : base(manager)
		{
		}

		public override int Count
		{
			get
			{
				return 3;
			}
		}

		public override Fragment GetItem(int position)
		{
			switch (position)
			{
				case 0:
					return new PopularFragment();
				case 1:
					return new MyCollectionFragment();
				case 2:
					return new SearchFragment();
				default:
					return null;
			}
		}
	}
}

