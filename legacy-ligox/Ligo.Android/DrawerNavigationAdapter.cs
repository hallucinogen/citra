using System;
using System.Collections.Generic;
using Android.Content;
using Android.Content.Res;
using Android.Views;
using Android.Widget;

namespace Ligo
{
	internal class DrawerNavigationAdapter : BaseAdapter<DrawerNavigationItem>
	{
		private Context context;
		private List<DrawerNavigationItem> items;

		public DrawerNavigationAdapter(Context context)
		{
			this.context = context;
			String[] navTitle = context.Resources.GetStringArray(Resource.Array.navigation_title);
			TypedArray navIcon = context.Resources.ObtainTypedArray(Resource.Array.navigation_icon);

			items = new List<DrawerNavigationItem>();
			for (int i = 0; i < navTitle.Length; ++i)
			{
				items.Add(new DrawerNavigationItem(navTitle[i], navIcon.GetResourceId(i, 0)));
			}
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override int Count
		{
			get
			{
				return items.Count;
			}
		}

		public override DrawerNavigationItem this [int index]
		{
			get
			{
				return items[index];
			}
		}

		public override int ViewTypeCount
		{
			get
			{
				return 1;
			}
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			if (convertView == null)
			{
				convertView = LayoutInflater.From(context).Inflate(Resource.Layout.DrawerItem, parent, false);
			}

			ImageView image = (ImageView)convertView.FindViewById(Resource.Id.icon);
			TextView text = (TextView)convertView.FindViewById(Resource.Id.text);

			DrawerNavigationItem item = this[position];
			image.SetImageResource(item.IconResource);
			text.Text = item.Title;

			return convertView;
		}
	}
}

