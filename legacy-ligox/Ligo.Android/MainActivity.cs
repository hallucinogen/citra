using System;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V4.App;

namespace Ligo
{
	[Android.App.Activity(Label = "Ligopace", MainLauncher = true)]
	public class MainActivity : FragmentActivity
	{
		private class DrawerNavigationToggle : ActionBarDrawerToggle
		{
			public class ActionBarDrawerEventArgs : EventArgs
			{
				public View DrawerView { get; set; }

				public float SlideOffset { get; set; }

				public int NewState { get; set; }
			}

			public delegate void ActionBarDrawerChangedEventHandler(object s,ActionBarDrawerEventArgs e);

			public DrawerNavigationToggle(Android.App.Activity activity,
				DrawerLayout drawerLayout,
				int drawerImageRes,
				int openDrawerContentDescRes,
				int closeDrawerContentDescRes)
				: base(activity,
					drawerLayout,
					drawerImageRes,
					openDrawerContentDescRes,
					closeDrawerContentDescRes)
			{

			}

			public event ActionBarDrawerChangedEventHandler DrawerClosed;
			public event ActionBarDrawerChangedEventHandler DrawerOpened;
			public event ActionBarDrawerChangedEventHandler DrawerSlide;
			public event ActionBarDrawerChangedEventHandler DrawerStateChanged;

			public override void OnDrawerClosed(View drawerView)
			{
				if (null != DrawerClosed)
					DrawerClosed(this, new ActionBarDrawerEventArgs { DrawerView = drawerView });
				base.OnDrawerClosed(drawerView);
			}

			public override void OnDrawerOpened(View drawerView)
			{
				if (null != DrawerOpened)
					DrawerOpened(this, new ActionBarDrawerEventArgs { DrawerView = drawerView });
				base.OnDrawerOpened(drawerView);
			}

			public override void OnDrawerSlide(View drawerView, float slideOffset)
			{
				if (null != DrawerSlide)
					DrawerSlide(this, new ActionBarDrawerEventArgs
					{
						DrawerView = drawerView,
						SlideOffset = slideOffset
					});
				base.OnDrawerSlide(drawerView, slideOffset);
			}

			public override void OnDrawerStateChanged(int newState)
			{
				if (null != DrawerStateChanged)
					DrawerStateChanged(this, new ActionBarDrawerEventArgs
					{
						NewState = newState
					});
				base.OnDrawerStateChanged(newState);
			}
		}

		private ListView listView;
		private DrawerLayout drawerLayout;
		private DrawerNavigationAdapter navigationAdapter;
		private DrawerNavigationToggle drawerToggle;

    private PopularFragment popularFragment = new PopularFragment();
    private MyCollectionFragment myCollectionFragment = new MyCollectionFragment();

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.Main);

			this.drawerLayout = FindViewById(Resource.Id.drawer_layout).JavaCast<DrawerLayout>();
			this.listView = FindViewById(Resource.Id.left_drawer).JavaCast<ListView>();

			// Set drawer layout and listener
			this.drawerToggle = new DrawerNavigationToggle(
				                                       this, drawerLayout,
				                                       Resource.Drawable.Icon,
				                                       Resource.String.app_name,
				                                       Resource.String.app_name
			                                       );
			this.drawerToggle.DrawerClosed += (object s, DrawerNavigationToggle.ActionBarDrawerEventArgs e) => {
				InvalidateOptionsMenu();
			};
			this.drawerToggle.DrawerOpened += (object s, DrawerNavigationToggle.ActionBarDrawerEventArgs e) => {
				InvalidateOptionsMenu();
			};
			drawerLayout.SetDrawerListener(drawerToggle);

			// Set list view and adapter
			this.navigationAdapter = new DrawerNavigationAdapter(this);
			this.listView.Adapter = navigationAdapter;
			this.listView.ItemClick += NavigationClick;

			// Set action bar home button to navigation
			this.ActionBar.SetDisplayHomeAsUpEnabled(true);
			this.ActionBar.SetHomeButtonEnabled(true);

      // Set initial displayed fragment
      this.SupportFragmentManager.BeginTransaction().Replace(Resource.Id.content_frame, popularFragment).Commit();
      listView.SetItemChecked(0, true);
		}

		protected override void OnPostCreate(Bundle savedInstanceState)
		{
			base.OnPostCreate(savedInstanceState);
			this.drawerToggle.SyncState();
		}

		public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
		{
			base.OnConfigurationChanged(newConfig);
			this.drawerToggle.OnConfigurationChanged(newConfig);
		}

		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			if (this.drawerToggle.OnOptionsItemSelected(item))
			{
				return true;
			}

			return base.OnOptionsItemSelected(item);
		}

		void NavigationClick(object sender, AdapterView.ItemClickEventArgs e)
		{
			Fragment targetFragment = null;
			switch (e.Position)
			{
				case 0:
          targetFragment = popularFragment;
					break;
				case 1:
          //targetFragment = myCollectionFragment;
					break;
				case 2:
          //targetFragment = myCollectionFragment;
					break;
				default:
					targetFragment = null;
					break;
			}

			this.SupportFragmentManager.BeginTransaction().Replace(Resource.Id.content_frame, targetFragment).Commit();
			listView.SetItemChecked(e.Position, true);
			drawerLayout.CloseDrawer(listView);
			this.ActionBar.Title = navigationAdapter[e.Position].Title;
		}
	}
}


