using System;
using Android.App;
using Android.Runtime;
using Parse;

namespace Ligo
{
	[Application]
	public class App : Application
	{
		public App (IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
		{
		}

		public override void OnCreate ()
		{
			base.OnCreate ();

			LigoClient.Initialize();
		}
	}
}
