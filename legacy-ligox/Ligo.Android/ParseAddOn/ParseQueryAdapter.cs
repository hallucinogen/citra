using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Android.Content;
using Android.Database;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Views;
using Android.Widget;
using Parse;

namespace Parse
{
	public class ParseQueryAdapter<T> : BaseAdapter<T> where T : ParseObject
	{
		public delegate void LoadingEventHandler();

		public delegate void LoadedEventHandler(IEnumerable<T> objects,AggregateException e);

		public delegate ParseQuery<T> CreateQueryDelegate();

		public Context Context
		{
			get
			{
				return context;
			}
		}

		private Context context;

		public string TextKey
		{
			set
			{
				textKey = value;
			}
		}

		private string textKey;

		public string ImageKey
		{
			set
			{
				imageKey = value;
			}
		}

		private string imageKey;

		public int ObjectsPerPage
		{
			get
			{
				return objectsPerPage;
			}
			set
			{
				objectsPerPage = value;
			}
		}

		private int objectsPerPage = 15;

		public bool PaginationEnabled
		{
			set
			{
				paginationEnabled = value;
			}
		}

		private bool paginationEnabled = true;
		private Drawable placeholder;
		private bool autoload = true;
		private List<T> objects = new List<T>();
		private int currentPage = 0;
		private int itemResourceId;
		private bool hasNextPage = true;
		private CreateQueryDelegate createQueryCallback;

		private event LoadingEventHandler Loading;
		private event LoadedEventHandler Loaded;

		private static readonly int VIEW_TYPE_ITEM = 0;
		private static readonly int VIEW_TYPE_NEXT_PAGE = 1;

		public ParseQueryAdapter(Context context, string className) : this(context, () =>
		{
			return new ParseQuery<T>(className);
		}, 0)
		{

		}

		public ParseQueryAdapter(Context context, string className, int itemViewResource) : this(context, () =>
		{
			return new ParseQuery<T>(className);
		}, itemViewResource)
		{

		}

		public ParseQueryAdapter(Context context, CreateQueryDelegate createQueryCallback) : this(context, createQueryCallback, 0)
		{

		}

		public ParseQueryAdapter(Context context, CreateQueryDelegate createQueryCallback, int itemViewResource)
		{
			this.context = context;
			this.createQueryCallback = createQueryCallback;
			this.itemResourceId = itemViewResource;
		}

		public override T this [int index]
		{
			get
			{
				if (index == this.GetPaginationCellRow())
				{
					return null;
				}
				return this.objects[index];
			}
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override int GetItemViewType(int position)
		{
			if (position == this.GetPaginationCellRow())
			{
				return VIEW_TYPE_NEXT_PAGE;
			}
			return VIEW_TYPE_ITEM;
		}

		public override int ViewTypeCount
		{
			get
			{
				return 2;
			}
		}

		public override void RegisterDataSetObserver(Android.Database.DataSetObserver observer)
		{
			base.RegisterDataSetObserver(observer);
			if (this.autoload)
			{
				this.LoadObjects();
			}
		}

		public override void UnregisterDataSetObserver(Android.Database.DataSetObserver observer)
		{
			base.UnregisterDataSetObserver(observer);
			observer = null;
		}

		public void Clear()
		{
			this.objects.Clear();
			this.NotifyDataSetChanged();
			this.currentPage = 0;
		}

		public void LoadObjects()
		{
			this.LoadObjects(0, true);
		}

		public void LoadObjects(int page, bool shouldClear)
		{
			ParseQuery<T> query = createQueryCallback();
			if (this.objectsPerPage > 0 && this.paginationEnabled)
			{
				this.SetPageOnQuery(page, query);
			}

			this.NotifyOnLoadingEventHandlers();

      query.FindAsync().ContinueWith(t =>
			{
				IEnumerable<T> foundObjects = t.Result;
				// TODO: add cache miss
				
				if (t.IsCanceled || t.IsFaulted)
				{
					hasNextPage = true;
				}
				else if (foundObjects != null)
				{
					this.currentPage = page;

					int foundObjectCount = foundObjects.Count();
					hasNextPage = (foundObjectCount > this.objectsPerPage);
					if (this.paginationEnabled && hasNextPage)
					{
						foundObjects = foundObjects.Take(this.objectsPerPage);
					}

					if (shouldClear)
					{
						this.objects.Clear();
					}

					this.objects.AddRange(foundObjects);

          ((Android.App.Activity)this.context).RunOnUiThread( () => {
					  NotifyDataSetChanged();
          });
				}

				NotifyOnLoadedEventHandlers(foundObjects, t.Exception);
			});
		}

		public void LoadNextPage()
		{
			this.LoadObjects(this.currentPage + 1, false);
		}

		public override int Count
		{
			get
			{
				int count = this.objects.Count;

				if (this.ShouldShowPaginationCell())
				{
					count++;
				}

				return count;
			}
		}

		protected virtual View GetItemView(T obj, View v, ViewGroup parent)
		{

			if (v == null)
			{
				v = this.GetDefaultView(this.context);
			}

			TextView textView = null;
			try
			{
				textView = (TextView)v.FindViewById(Android.Resource.Id.Text1);
			}
			catch (Exception e)
			{

			}

			if (textView != null)
			{
				if (this.textKey == null)
				{
					textView.Text = obj.ObjectId;
				}
				else if (obj[this.textKey] != null)
				{
					textView.Text = obj[this.textKey].ToString();
				}
				else
				{
					textView.Text = null;
				}
			}

			if (this.imageKey != null)
			{
				ParseImageView imageView = null;
				try
				{
					imageView = (ParseImageView)v.FindViewById(Android.Resource.Id.Icon);
				}
				catch (Exception e)
				{

				}

				if (imageView == null)
				{

				}

				imageView.SetPlaceholder(this.placeholder);
				imageView.SetParseFile((ParseFile)obj[imageKey]);
				imageView.LoadInBackground();
			}

			return v;
		}

		public View GetNextPageView(View v, ViewGroup parent)
		{
			if (v == null)
			{
				v = this.GetDefaultView(this.context);
			}
			TextView textView = (TextView)v.FindViewById(Android.Resource.Id.Text1);
			textView.Text = "Load more...";
			return v;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
    {
			if (this.GetItemViewType(position) == VIEW_TYPE_NEXT_PAGE)
			{
				View nextPageView = this.GetNextPageView(convertView, parent);
				nextPageView.Click -= ClickListener;
				nextPageView.Click += ClickListener;
				return nextPageView;
			}
			return this.GetItemView(this[position], convertView, parent);
		}

		private void ClickListener(object sender, EventArgs e) {
			LoadNextPage();
		}

		protected void SetPageOnQuery(int page, ParseQuery<T> query)
		{
			query.Limit(this.objectsPerPage + 1);
			query.Skip(page * this.objectsPerPage);
		}

		public void SetPlaceholder(Drawable placeholder)
		{
			if (this.placeholder == placeholder)
			{
				return;
			}
			this.placeholder = placeholder;

		}

		public void SetAutoLoad(bool autoload)
		{
			if (this.autoload == autoload)
			{
				return;
			}
			this.autoload = autoload;
			if (this.autoload && this.objects.Count == 0)
			{
				this.LoadObjects();
			}
		}

		private View GetDefaultView(Context context)
		{
			if (this.itemResourceId != 0)
			{
				return View.Inflate(context, this.itemResourceId, null);
			}
			LinearLayout view = new LinearLayout(context);
			view.SetPadding(8, 4, 8, 4);

			ParseImageView imageView = new ParseImageView(context);
			imageView.Id = Android.Resource.Id.Icon;
			imageView.LayoutParameters = new LinearLayout.LayoutParams(50, 50);
			view.AddView(imageView);

			TextView textView = new TextView(context);
			textView.Id = Android.Resource.Id.Text1;
			textView.LayoutParameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FillParent, ViewGroup.LayoutParams.WrapContent);
			textView.SetPadding(8, 0, 0, 0);
			view.AddView(textView);

			return view;
		}

		private int GetPaginationCellRow()
		{
			return this.objects.Count;
		}

		private bool ShouldShowPaginationCell()
		{
			return this.paginationEnabled && this.objects.Count > 0 && this.hasNextPage;
		}

		private void NotifyOnLoadingEventHandlers()
		{
			if (this.Loading != null)
			{
				this.Loading();
			}
		}

		private void NotifyOnLoadedEventHandlers(IEnumerable<T> objects, AggregateException e)
		{
			if (this.Loaded != null)
			{
				this.Loaded(objects, e);
			}
		}
	}
}

