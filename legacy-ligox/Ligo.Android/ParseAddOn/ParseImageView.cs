using System;
using System.Threading.Tasks;
using System.Net;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Util;
using Android.Widget;

namespace Parse
{
	public class ParseImageView : ImageView
	{
		private ParseFile file;
		private Drawable placeholder;
		private bool isLoaded = true;

		public ParseImageView(Context context) : base(context)
		{
		}

		public ParseImageView(Context context, IAttributeSet attributeSet) : base(context, attributeSet)
		{
		}

		public override void SetImageBitmap(Bitmap bm)
		{
			base.SetImageBitmap(bm);
			this.isLoaded = true;
		}

		public void SetPlaceholder(Drawable placeholder)
		{
			this.placeholder = placeholder;
			if (!this.isLoaded)
			{
				this.SetImageDrawable(this.placeholder);
			}
		}

		public void SetParseFile(ParseFile file)
		{
			this.isLoaded = false;
			this.file = file;
			this.SetImageDrawable(this.placeholder);
		}

		public Task LoadInBackground()
		{
			if (this.file == null)
			{
        return Task.FromResult(0);
			}

			WebClient client = new WebClient();
      return client.DownloadDataTaskAsync(this.file.Url).ContinueWith( t => {
        var imageBytes = t.Result;
        using (var bitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length))
        {
          if (bitmap != null)
          {
            SetImageBitmap(bitmap);
          }
        }
        return;
      });
		}
	}
}

