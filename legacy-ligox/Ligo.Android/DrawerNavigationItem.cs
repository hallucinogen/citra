using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Ligo
{
	internal class DrawerNavigationItem
	{
		private string title;

		public string Title { get { return title; } }

		private int iconResource;

		public int IconResource { get { return iconResource; } }

		public DrawerNavigationItem(String title, int iconResource)
		{
			this.title = title;
			this.iconResource = iconResource;
		}
	}
}

