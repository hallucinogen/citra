using System;
using Parse;

namespace Ligo
{
	public class LigoClient
	{
		public LigoClient()
		{

		}

		public static void Initialize() {
			ParseObject.RegisterSubclass<Category>();
			ParseObject.RegisterSubclass<Image>();
			ParseObject.RegisterSubclass<Payment>();
			ParseObject.RegisterSubclass<Wallpaper>();
			// Initialize the Parse client with your Application ID and .NET Key found on
			// your Parse dashboard
			ParseClient.Initialize(Constants.APP_KEY, Constants.NET_KEY);
		}
	}
}

