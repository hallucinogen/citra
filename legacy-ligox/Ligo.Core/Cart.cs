using System;
using Parse;

namespace Ligo
{
  [ParseClassName("Cart")]
  public class Cart : ParseObject
	{
    [ParseFieldName("status")]
    public int Status {
      get { return GetProperty<int>(); }
      set { SetProperty(value); }
    }

    [ParseFieldName("user")]
    public ParseUser User {
      get { return GetProperty<ParseUser>(); }
      set { SetProperty(value); }
    }

    [ParseFieldName("string")]
    public string Shipping {
      get { return GetProperty<string>(); }
      set { SetProperty(value); }
    }
  }
}

