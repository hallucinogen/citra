using System;
using Parse;

namespace Ligo
{
	[ParseClassName("Payment")]
	public class Payment : ParseObject
	{
    [ParseFieldName("serverPaymentId")]
    public string ServerPaymentId {
      get { return GetProperty<string>(); }
      set { SetProperty(value); }
    }

    [ParseFieldName("status")]
    public int Status {
      get { return GetProperty<int>(); }
      set { SetProperty(value); }
    }
	}
}

