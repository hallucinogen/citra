using System;
using Parse;

namespace Ligo
{
	[ParseClassName("Wallpaper")]
	public class Wallpaper : ParseObject
	{
		[ParseFieldName("description")]
		public string Description {
			get { return GetProperty<string>(); }
			set { SetProperty(value); }
		}

		[ParseFieldName("image")]
		public Image Image {
			get { return GetProperty<Image>(); }
			set { SetProperty(value); }
		}

		[ParseFieldName("title")]
		public string Title {
			get { return GetProperty<string>(); }
			set { SetProperty(value); }
		}

		[ParseFieldName("user")]
		public ParseUser User {
			get { return GetProperty<ParseUser>(); }
			set { SetProperty(value); }
		}

		[ParseFieldName("price")]
		public double Price {
			get { return GetProperty<double>(); }
			set { SetProperty(value); }
		}

		public Wallpaper()
		{
		}
	}
}

