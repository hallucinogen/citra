using System;
using Parse;

namespace Ligo
{
	[ParseClassName("Category")]
	public class Category : ParseObject
	{
		[ParseFieldName("name")]
		public string Name {
			get { return GetProperty<string>(); }
			set { SetProperty(value); }
		}

		public Category()
		{
		}
	}
}

