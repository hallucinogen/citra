using System;
using Parse;

namespace Ligo
{
	[ParseClassName("Image")]
	public class Image : ParseObject
	{
		[ParseFieldName("image")]
		public ParseFile ImageFile {
			get { return GetProperty<ParseFile>(); }
			set { SetProperty(value); }
		}

		[ParseFieldName("wallpaper")]
    public Wallpaper Wallpaper {
			get { return GetProperty<Wallpaper>(); }
			set { SetProperty(value); }
		}

		[ParseFieldName("dimension")]
    public Tuple<int, int> Dimension {
			get {
        string val = GetProperty<string>();
        char[] delimiter = { ',' };
        string[] values = val.Split(delimiter);
        return new Tuple<int, int>(Int32.Parse(values[0]), Int32.Parse(values[1]));
      }
			set {
        SetProperty(value.Item1.ToString() + "," + value.Item2.ToString());
      }
		}

		public Image ()
		{
		}
	}
}

