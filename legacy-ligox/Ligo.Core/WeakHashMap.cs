using System;
using System.Collections.Generic;

namespace Ligo
{
	public class WeakHashMap<TKey, TValue>
		where TKey : class
		where TValue : class
	{
		private class Reference
		{
			private int hashCode;

			public WeakReference WeakReference { get; private set; }

			public Reference(TKey key)
			{
				this.hashCode = key.GetHashCode();
				WeakReference = new WeakReference(key);
			}

			public TKey Value
			{
				get
				{
					return (TKey)WeakReference.Target;
				}
			}

			public override int GetHashCode()
			{
				return this.hashCode;
			}

			public override bool Equals(object obj)
			{
				var otherRef = obj as Reference;
				if (otherRef == null)
				{
					return false;
				}
				if (otherRef.GetHashCode() != this.GetHashCode())
				{
					return false;
				}
				return object.ReferenceEquals(otherRef.WeakReference.Target, this.WeakReference.Target);
			}
		}

		private IDictionary<Reference, TValue> data;

		public WeakHashMap()
		{
			this.data = new Dictionary<Reference, TValue>();
		}

		private void CleanUp()
		{
			foreach (var key in this.data.Keys)
			{
				if (key.WeakReference.IsAlive)
				{
					data.Remove(key);
				}
			}
		}

		public TValue this [TKey key]
		{
			get
			{
				this.CleanUp();
				var reference = new Reference(key);
				TValue value;
				if (this.data.TryGetValue(reference, out value))
				{
					return value;
				}
				return null;
			}
			set
			{
				this.CleanUp();
				var reference = new Reference(key);
				this.data[reference] = value;
			}
		}

		public int Count
		{
			get
			{
				this.CleanUp();
				return this.data.Count;
			}
		}

		public ICollection<TKey> Keys
		{
			get
			{
				var keys = this.data.Keys;

				ICollection<TKey> res = new List<TKey>();
				foreach (var key in keys)
				{
					if (key.WeakReference.IsAlive && key.WeakReference.Target != null)
					{
						res.Add((TKey)key.WeakReference.Target);
					}
				}

				return res;
			}
		}
	}
}

