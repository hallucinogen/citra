﻿using System;
using System.Diagnostics;
using Android.App;
using Android.Runtime;
//using Parse;

namespace LigoMobile.Android
{
  [Application]
  public class LigoApp : Application
  {
    public LigoApp (IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
    {
    }

    public override void OnCreate() {
      base.OnCreate();
      App.Initialize();
    }
  }
}

