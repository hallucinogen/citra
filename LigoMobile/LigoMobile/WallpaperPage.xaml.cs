﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Parse;
using System.Collections.Generic;
using System.Linq;

namespace LigoMobile
{	
	public partial class WallpaperPage : ContentPage
	{	
    private static object locker = new object();
    private ListView listView;
    private List<Wallpaper> wallpapers = new List<Wallpaper>();

		public WallpaperPage ()
		{
			InitializeComponent ();

      Title = "Wallpaper";
      Icon = "wallpaper.png";

      listView = new ListView();
      Content = listView;

      var template = new DataTemplate(() => {
        System.Diagnostics.Debug.WriteLine("trying to download");
        var image = new Xamarin.Forms.Image();
        image.SetBinding(Xamarin.Forms.Image.SourceProperty, new Binding("ImageUrl"));
        System.Diagnostics.Debug.WriteLine("trying to download");
        return new ViewCell() {
          View = image
        };
      });
      listView.ItemTemplate = template;

      //listView.ItemsSource = wallpapers;

      LoadImages();
		}

    protected override void OnAppearing()
    {
      base.OnAppearing();
    }

    public void LoadImages() {
      var query = new ParseQuery<Wallpaper>();
      query.Include("image");


      lock (locker)
      {
        query.FindAsync().ContinueWith(t =>
        {
          var results = t.Result;
          int cnt = 0;
          int cnt2 = 0;

          foreach (var res in results)
          {
            if (cnt > 4)
            {
              break;
            }
            ++cnt;
            //System.Diagnostics.Debug.WriteLine("kotoran");
            res.Image.FetchAsync<Image>().ContinueWith(t2 =>
            {
              cnt2++;
              res.Image = t2.Result;
              System.Diagnostics.Debug.WriteLine("cumi");
              wallpapers.Add(res);
            });
          }
        });
      }

      lock (locker)
      {
        System.Diagnostics.Debug.WriteLine("gigo");
        listView.ItemsSource = wallpapers;
      }
    }
	}
}

