﻿using System;
using Parse;

namespace LigoMobile
{
  [ParseClassName("Wallpaper")]
  public class Wallpaper : ParseObject
  {
    public Wallpaper()
    {
    }

    [ParseFieldName("description")]
    public string Description {
      get { return GetProperty<string>(); }
      set { SetProperty(value); }
    }

    [ParseFieldName("image")]
    public Image Image {
      get { return GetProperty<Image>(); }
      set { SetProperty(value); }
    }

    [ParseFieldName("price")]
    public double Price {
      get { return GetProperty<double>(); }
      set { SetProperty(value); }
    }

    [ParseFieldName("title")]
    public string Title {
      get { return GetProperty<string>(); }
      set { SetProperty(value); }
    }

    [ParseFieldName("user")]
    public ParseUser User {
      get { return GetProperty<User>(); }
      set { SetProperty(value); }
    }

    [ParseFieldName("views")]
    public int Views {
      get { return GetProperty<int>();  }
      set { SetProperty(value); }
    }

    public Uri ImageUrl {
      get { return this.Image.ImageFile.Url; }
    }
  }
}

