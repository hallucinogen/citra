﻿using System;
using Parse;

namespace LigoMobile
{
  [ParseClassName("Image")]
  public class Image : ParseObject
  {
    public Image()
    {
    }

    [ParseFieldName("image")]
    public ParseFile ImageFile {
      get { return GetProperty<ParseFile>();  }
      set { SetProperty(value); }
    }

    [ParseFieldName("type")]
    public string Type {
      get { return GetProperty<string>(); }
      set { SetProperty(value); }
    }

    [ParseFieldName("wallpaper")]
    public Wallpaper Wallpaper {
      get { return GetProperty<Wallpaper>(); }
      set { SetProperty(value); }
    }
  }
}

