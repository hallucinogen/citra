﻿using System;
using Parse;

namespace LigoMobile
{
  [ParseClassName("CartItem")]
  public class CartItem : ParseObject
  {
    public CartItem()
    {
    }

    [ParseFieldName("cart")]
    public Cart Cart {
      get { return GetProperty<Cart>(); }
      set { SetProperty(value); }
    }

    [ParseFieldName("wallpaper")]
    public Wallpaper Wallpaper {
      get { return GetProperty<Wallpaper>(); }
      set { SetProperty(value); }
    }
  }
}

