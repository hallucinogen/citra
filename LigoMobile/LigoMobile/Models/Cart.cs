﻿using System;
using Parse;

namespace LigoMobile
{
  [ParseClassName("Cart")]
  public class Cart : ParseObject
  {
    public Cart()
    {
    }

    [ParseFieldName("status")]
    public string Status {
      get { return GetProperty<string>(); }
      set { SetProperty(value); }
    }

    [ParseFieldName("paypalToken")]
    public string PaypalToken {
      get { return GetProperty<string>(); }
      set { SetProperty(value); }
    }

    [ParseFieldName("user")]
    public User User {
      get { return GetProperty<User>(); }
      set { SetProperty(value); }
    }
  }
}

