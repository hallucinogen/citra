﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace LigoMobile
{	
	public partial class MyCollectionPage : ContentPage
	{	
		public MyCollectionPage ()
		{
			InitializeComponent ();

      Title = "My Collection";
      Icon = "my_collection.png";
		}
	}
}

