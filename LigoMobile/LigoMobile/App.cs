﻿using System;
using System.Linq;
using System.Collections.Generic;
using Xamarin.Forms;
using Parse;

namespace LigoMobile
{
  public class App
  {
    public static Page GetMainPage()
    {	
      return new MainPage();
    }

    public static void Initialize() {
      ParseObject.RegisterSubclass<Cart>();
      ParseObject.RegisterSubclass<CartItem>();
      ParseObject.RegisterSubclass<Image>();
      ParseObject.RegisterSubclass<Wallpaper>();
      ParseClient.Initialize("ZIqbEBf3PXXSzpbQbvz5BcmVcK54DjSmKwNxCah1", "EY4UfiJn0xGhvgvNhBRrY6kggn4nv9zGGk5klQQo");
    }
  }
}

