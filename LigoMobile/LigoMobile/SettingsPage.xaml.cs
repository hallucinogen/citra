﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace LigoMobile
{	
	public partial class SettingsPage : ContentPage
	{	
		public SettingsPage ()
		{
			InitializeComponent ();

      Title = "Settings";
      Icon = "settings.png";
		}
	}
}

