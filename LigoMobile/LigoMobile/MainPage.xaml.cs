﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace LigoMobile
{	
  public partial class MainPage : TabbedPage
	{	
		public MainPage ()
		{
			InitializeComponent ();

      Title = "Ligopace";

      Children.Add(new WallpaperPage());
      Children.Add(new LiveWallpaperPage());
      Children.Add(new MyCollectionPage());
      Children.Add(new SettingsPage());
		}
	}
}

