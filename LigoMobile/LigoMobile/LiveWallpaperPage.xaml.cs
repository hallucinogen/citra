﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace LigoMobile
{	
	public partial class LiveWallpaperPage : ContentPage
	{	
		public LiveWallpaperPage ()
		{
			InitializeComponent ();

      Title = "Live";
      Icon = "live.png";
		}
	}
}

