var Image = require("parse-image");

/*
  Resizes an image from one Parse Object key containing
  a Parse File to a file object at a new key with a target width.
  If the image is smaller than the target width, then it is simply
  copied unaltered.
 
  fromNbject: Parse Object
  toObject: Parse Object
  fromKey: Key containing the source Parse File
  toKey: Key to contain the target Parse File
  width: Target width
  height: Target height
  crop: Center crop the square
*/
module.exports = function(options) {
  var format, originalHeight, originalWidth;

  // First get the image data
  return Parse.Cloud.httpRequest({
    url: options.fromObject.get(options.fromKey).url()
  }).then(function(response) {
    var image = new Image();
    return image.setData(response.buffer);
  }).then(function(image) {
    // set some metadata that will be on the object
    format = image.format();
    originalHeight = image.height();
    originalWidth = image.width();

    if (options.width && !options.height && image.width() <= options.width) {
      // No need to resize
      return new Parse.Promise.as(image);
    } else if (options.height && !options.width && image.height() <= options.height) {
      // No need to resize
      return new Parse.Promise.as(image);
    } else {
      var newWidth = options.width ? options.width : options.height * image.width() / image.height();
      var newHeight = options.height ? options.height :options.width * image.height() / image.width();
 
      return image.scale({
        width: newWidth,
        height: newHeight
      });
    }
  }).then(function(image) {
    // Get the image data in a Buffer.
    return image.data();
  }).then(function(buffer) {
    // Save the image into a new file.
    var base64 = buffer.toString("base64");
    var scaled = new Parse.File("image." + format, { base64: base64 });
    return scaled.save();
  }).then(function(image) {
    // Set metadata on the image object
    options.toObject.set(options.toKey, image);
  });
};
