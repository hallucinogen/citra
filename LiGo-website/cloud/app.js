var express = require('express');
var wallpaper = require('cloud/routes/wallpaper');
var expressLayouts = require('cloud/express-layouts');
var user = require('cloud/routes/user');
var payment = require('cloud/routes/payment');
var parseExpressCookieSession = require('parse-express-cookie-session');
var parseExpressHttpsRedirect = require('parse-express-https-redirect');

var app = express();

// Basic auth
function checkAuth(req, res, next) {
  if (Parse.User.current()) {
    next();

  } else {
    res.redirect("/login");
  }
}

// Configure the app
app.set('views', 'cloud/views');
app.set('view engine', 'ejs');
app.use(expressLayouts);
app.use(parseExpressHttpsRedirect());
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('_ligobeat'));
app.use(parseExpressCookieSession({
  fetchUser: true,
  key: 'wallpaper.sess',
  cookie: {
    maxAge: 3600000 * 24 * 30
  }
}));

app.locals.parseApplicationId = 'ZIqbEBf3PXXSzpbQbvz5BcmVcK54DjSmKwNxCah1';
app.locals.parseJavascriptKey = 'VcGaMMMeX261fCfRQG7DG1Imb0WmjoFeesN1ZEAc';
app.locals.facebookApplicationId = '552516634830588';

// Setup underscore to be available in all templates
app.locals._ = Parse._;
app.locals.humanize = require('cloud/humanize');

// Always inject cart count
app.use(wallpaper.injectCart);

// Define all the endpoints

app.post('/signup', user.doSignup);
app.post('/login', user.doLogin);
app.post('/logout', user.doLogout);
app.get('/signup', user.signup);
app.get('/login', user.login);
app.get('/logout', user.doLogout);

app.post('/wallpaper', checkAuth, wallpaper.doCreate);
app.post('/wallpaper/:objectId', wallpaper.ownAuth, wallpaper.doUpdate);
app.get('/wallpaper/new', checkAuth, wallpaper.create);
app.get('/wallpaper/:objectId', wallpaper.show);
app.get('/wallpaper', wallpaper.popular);

// Carts
app.post('/cart', wallpaper.doAddCart);
app.post('/cart-delete', wallpaper.deleteCartItem);
app.get('/cart', wallpaper.cart);
app.get('/favorites', wallpaper.favorites);

// Payment
app.post('/payment', payment.doPay);
app.get('/payment/cancel', payment.paymentFail);
app.get('/payment', payment.paymentSuccess);

app.get('/', wallpaper.popular);

app.listen(); 
