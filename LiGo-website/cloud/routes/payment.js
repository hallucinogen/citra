var paypal = require('cloud/paypal');
var querystring = require('querystring');
var _ = Parse._;

var createPaymentJSONTemplate = {
  "intent": "sale",
  "payer": {
    "payment_method": "paypal"
  },
  "redirect_urls": {
    "return_url": "https://www.ligopace.com/payment",
    "cancel_url": "https://www.ligopace.com/payment/cancel"
  },
  "transactions": []
};

// development
paypal.configure({
  "host" : "api.sandbox.paypal.com",
  "client_id" : "ASf-WxBeXzV6GHD_Qxpj7JNfeIq8qqoNFrYkjOkjGoauhmc-pmtGW4GY-18V",
  "client_secret" : "ELslJBBWL9YCwEO7uiQXKmALf85-lk0wCqSkELczn3wUc_EgaSLNSh3QTp5x"
});

// production
/*paypal.configure({
  "host" : "api.paypal.com",
  "port" : "",            
  "client_id" : "AbO4FBB245fU-pZhhRg_UaBmNUmjMb-Re3OnfejsIpUW0IqO2PhptfXFSH9Y",
  "client_secret" : "EMiETRBFvQaTGeVu5oitzRgs-JojSKZ5riwgaXVysCMxRQqrStvRd1AElZWy"
});*/

exports.paymentSuccess = function(req, res) {
  var token = req.query.token;
  console.log(token);
  var query = new Parse.Query('Cart');
  query.equalTo('paypalToken', token);
  query.first().then(function(cart) {
    if (!cart) {
      // TODO: show error message
      return Parse.Promise.as();
    }

    if (cart.id != res.locals.cart.id) {
      // tampered cart
      // TODO: do something about tampered cart
      return Parse.Promise.as();
    }

    cart.set('status', 'complete');
    // TODO: set the wallpaper possession to user
    // TODO: show success message, maybe we can redirect somewhere else
    return cart.save();
  }).then(function() {
    res.redirect('/cart');
  });
};

exports.paymentFail = function(req, res) {
  // TODO: show error message that payment is cancelled
  res.redirect('/cart');
};

exports.doPay = function(req, res) {
  var items = res.locals.cartItems;
  var cart = res.locals.cart;
  var sum = 0;
  var itemList = [];
  _.each(items, function(item) {
    sum += parseFloat(item.get('wallpaper').get('price'));
    itemList.push({
      name: item.get('wallpaper').get('title'),
      sku: 'item',
      price: item.get('wallpaper').get('price'),
      quantity: 1,
      currency: 'USD'
    });
  });
  var amount = {
    currency: 'USD',
    total: sum.toFixed(2)
  }
  var createPaymentJSON = _.clone(createPaymentJSONTemplate);
  createPaymentJSON.transactions.push({
    item_list: { items: itemList },
    description: 'Wallpapers payment',
    amount: amount
  });

  paypal.payment.create(createPaymentJSON, function(error, payment) {
    if (error) {
      res.send(502);
    } else {
      console.log('Create Payment Response');
      console.log(payment);

      var approvalLink = _.find(payment.links, function(link) {
        return link.rel === 'approval_url';
      });

      var paypalQuery = querystring.parse(approvalLink.href);

      cart.set('paypalToken', paypalQuery.token);
      cart.save().then(function() {
        res.redirect(approvalLink.href);
      });
    }
  });
};
