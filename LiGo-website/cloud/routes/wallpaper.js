var Wallpaper = Parse.Object.extend('Wallpaper');
var Image = Parse.Object.extend('Image');
var Favorite = Parse.Object.extend('Favorite');
var Cart = Parse.Object.extend('Cart');
var CartItem = Parse.Object.extend('CartItem');
var _ = Parse._;
var thumbnailGenerator = require('cloud/thumbnail-generator');

// Shows the list of wallpapers
exports.latest = function(req, res) {
  var query = new Parse.Query(Wallpaper);
  query.equalTo('status', 'approved');
  query.limit(28);
  query.descending('createdAt');
  query.include("image");

  query.find().then(function(wallpapers) {
    res.render('wallpaper/index', {
      title: "Popular Wallpapers",
      wallpapers: wallpapers,
      nav: 'wallpaper'
    });
  });
};

// Shows a list of popular wallpapers based on view count
exports.popular = function(req, res) {
  var query = new Parse.Query(Wallpaper);
  query.equalTo('status', 'approved');
  query.limit(28);
  query.descending('views');
  query.include("image");

  // TODO: get 5 feature image (feature will be handpicked)
  query.find().then(function(wallpapers) {
    res.render('wallpaper/index', {
      title: "Popular Wallpapers",
      wallpapers: wallpapers,
      nav: 'wallpaper'
    });
  });
};

// Create a new wallpaper page
exports.create = function(req, res) {
  res.render('wallpaper/new', {
    title: "Upload a Wallpaper"
  });
};

// Creates a wallpaper
exports.doCreate = function(req, res) {
  var wallpaper = new Wallpaper();
  var mainImage = new Image();
  var phoneImage = new Image();
  var tabletImage = new Image();
  var mainImageThumbnail = new Image();
  var phoneImageThumbnail = new Image();
  var tabletImageThumbnail = new Image();

  if (req.body.text1 === "") {
    res.status(403).send('Top text cannot be blank');
  }

  // Set metadata fields
  mainImage.set('image', req.body.mainFile);
  mainImage.set('type', 'main');
  phoneImage.set('image', req.body.phoneFile || req.body.mainFile);
  phoneImage.set('type', 'main');
  tabletImage.set('image', req.body.tabletFile || req.body.mainFile);
  tabletImage.set('type', 'main');
  mainImageThumbnail.set('type', 'mainThumbnail');
  phoneImageThumbnail.set('type', 'phoneThumbnail');
  tabletImageThumbnail.set('type', 'tabletThumbnail');

  // generate thumbnails for each platform
  var generateThumbnails = thumbnailGenerator({
    fromObject: mainImage,
    toObject: mainImageThumbnail,
    fromKey: 'image',
    toKey: 'image',
    height: 420
  }).then(function() {
    thumbnailGenerator({
      fromObject: phoneImage,
      toObject: phoneImageThumbnail,
      fromKey: 'image',
      toKey: 'image',
      height: 300
    });
  }).then(function() {
    thumbnailGenerator({
      fromObject: tabletImage,
      toObject: tabletImageThumbnail,
      fromKey: 'image',
      toKey: 'image',
      height: 320
    });
  });

  wallpaper.set('title', req.body.title);
  wallpaper.set('description', req.body.description);
  wallpaper.set('price', parseFloat(req.body.price).toFixed(2));
  wallpaper.set('status', 'inReview');

  // Wallpapers are read only
  var acl = new Parse.ACL();
  acl.setPublicReadAccess(true);
  if (Parse.User.current()) {
    wallpaper.set("user", Parse.User.current());
    acl.setWriteAccess(Parse.User.current(), true);
  }
  wallpaper.setACL(acl);
  mainImage.setACL(acl);
  mainImageThumbnail.setACL(acl);
  phoneImage.setACL(acl);
  phoneImageThumbnail.setACL(acl);
  tabletImage.setACL(acl);
  tabletImageThumbnail.setACL(acl);

  generateThumbnails.then(function() {
    return wallpaper.save();
  }).then(function(object) {
    wallpaper = object;
    mainImage.set('wallpaper', wallpaper);
    return mainImage.save();
  }).then(function() {
    mainImageThumbnail.set('wallpaper', wallpaper);
    return mainImageThumbnail.save();
  }).then(function() {
    phoneImage.set('wallpaper', wallpaper);
    return phoneImage.save();
  }).then(function() {
    phoneImageThumbnail.set('wallpaper', wallpaper);
    return phoneImageThumbnail.save();
  }).then(function() {
    tabletImage.set('wallpaper', wallpaper);
    return tabletImage.save();
  }).then(function() {
    tabletImageThumbnail.set('wallpaper', wallpaper);
    return tabletImageThumbnail.save();
  }).then(function(object) {
    wallpaper.set('image', object);
    return wallpaper.save();
  }).then(function() {
    res.send({ id: wallpaper.id });
  }, function(error) {
    res.send('Error saving wallpaper!');
  });
};

exports.doUpdate = function(req, res) {
  var objectId = req.params.objectId;
  var query = new Parse.Query(Wallpaper);
  query.get(objectId).then(function(wallpaper) {
    if (req.body.title) {
      wallpaper.set('title', req.body.title);
    }
    if (req.body.description) {
      wallpaper.set('description', req.body.description);
    }
    if (req.body.price) {
      wallpaper.set('price', parseFloat(req.body.price).toFixed(2));
    }
  });
}

// Shows a wallpaper
exports.show = function(req, res) {
  var objectId = req.params.objectId;
  var query = new Parse.Query(Wallpaper);
  query.include("image");

  query.get(objectId).then(function(wallpaper) {
    Parse.Cloud.useMasterKey();
    var baseUrl = req.headers.host;

    // Increment the view counter
    wallpaper.increment("views");
    wallpaper.set('price', parseFloat(wallpaper.get('price')).toFixed(2));
    wallpaper.save().then(function() {
      res.render('wallpaper/show', {
        wallpaper: wallpaper,
        title: "Show",
        wallpaperUrl: wallpaper.get('image').get('image').url(),
        baseUrl: baseUrl,
        nav: 'wallpaper'
      });
    }, function(error) {
      res.send(error);
    });
  }, function() {
    res.status(404).send("Wallpaper not found.");
  });
};

exports.search = function(req, res) {
  // TODO: do something
};

exports.doSearch = function(req, res) {
  // TODO: do something
};

var getCartItems = function(req, res) {
  if (res.locals.cartItems) {
    return Parse.Promise.as(res.locals.cartItems);
  }

  var currentUser = Parse.User.current();

  if (!currentUser) {
    // TODO: make anonymous user
    return Parse.Promise.as();
  } else {
    var query = new Parse.Query(Cart);
    query.equalTo('user', currentUser);
    query.equalTo('status', 'shopping');
    return query.first().then(function(cart) {
      if (cart) {
        return Parse.Promise.as(cart);
      }

      cart = new Cart();
      cart.set('user', currentUser);
      cart.set('status', 'shopping');
      return cart.save();
    }).then(function(cart) {
      res.locals.cart = cart;
      query = new Parse.Query(CartItem);
      query.equalTo('cart', cart);
      query.include('wallpaper');
      query.include('wallpaper.image');
      return query.find();
    });
  }
};

exports.injectCart = function(req, res, next) {
  getCartItems(req, res).then(function(items) {
    res.locals.cartItems = items || [];
    next();
  });
};

exports.cart = function(req, res) {
  var items = res.locals.cartItems;
  var sum = 0;
  _.each(items, function(item) {
    sum += parseFloat(item.get('wallpaper').get('price'));
  });
  res.render('wallpaper/cart', {
    title: 'Your Cart',
    total: sum
  });
};

exports.favorites = function(req, res) {

};

exports.doAddCart = function(req, res) {
  var isCart = req.body.cart;
  var isFav = req.body.favorites;
  var objectId = req.body.id;
  cart = null;
  wallpaper = null;

  if (!isCart && !isFav) {
    res.redirect('/wallpaper');
  }

  var query = new Parse.Query(Wallpaper);
  query.get(objectId).then(function(wallpaperResult) {
    wallpaper = wallpaperResult;

    if (!wallpaperResult) {
      // TODO: maybe flash "Item not found"
      res.redirect('/wallpaper');
    }

    var currentUser = Parse.User.current();
    if (isCart) {
      if (!currentUser) {
        // TODO: save it to cookie
      } else {
        query = new Parse.Query(Cart);
        query.equalTo('user', currentUser);
        query.equalTo('status', 'shopping');
        return query.first().then(function(cart) {
          if (!cart) {
            cart = new Cart();
            cart.set('user', currentUser);
            cart.set('status', 'shopping');

            return cart.save();
          }

          return Parse.Promise.as(cart);
        }).then(function(cartResult) {
          cart = cartResult;

          query = new Parse.Query(CartItem);
          query.equalTo('cart', cart);
          query.equalTo('wallpaper', wallpaper);
          return query.first();
        }).then(function(cartItem) {
          if (cartItem) {
            return Parse.Promise.as();
          }

          cartItem = new CartItem();
          cartItem.set('cart', cart);
          cartItem.set('wallpaper', wallpaper);
          return cartItem.save();
        }).then(function() {
          res.redirect('/cart');
        });
      }
    } else if (isFav) {
      if (!currentUser) {
        res.redirect("/login");
      } else {
        query = new Parse.Query(Favorite);
        query.equalTo('user', currentUser);
        query.equalTo('wallpaper', wallpaper);
        return query.find().then(function(fav) {
          if (!fav) {
            fav = new Favorite();
            fav.set('user', currentUser);
            fav.set('wallpaper', wallpaper);

            return fav.save();
          }
        }).then(function() {
          res.redirect('/favorites');
          return Parse.Promise.success();
        });
      }
    }
  }, function() {
    // TODO: flash error
    res.redirect('/wallpaper');
  });
};

exports.deleteCartItem = function(req, res) {
  var objectId = req.body.id;

  var query = new Parse.Query(CartItem);
  query.get(objectId).then(function(cartItem) {
    console.log(cartItem);
    return cartItem.destroy();
  }).then(function() {
    res.redirect('/cart');
  });
};

exports.ownAuth = function(req, res, next) {
  var currentUser = Parse.User.current();
  if (!currentUser) {
    res.redirect("/login");
  } else {
    var objectId = req.params.objectId;
    var query = new Parse.Query(Wallpaper);

    query.get(objectId).then(function(wallpaper) {
      var acl = wallpaper.getACL();
      // TODO: admin should be allowed too
      if (acl.getWriteAccess(currentUser)) {
        next();
      } else {
        res.send("You are not authorized to do this action");
      }
    });
  }
}
