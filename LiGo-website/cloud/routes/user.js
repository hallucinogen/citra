// render signup UI
exports.signup = function(req, res) {
  res.render('user/signup', {
    title: 'Sign Up', 
  });
};

// signup user via post
exports.doSignup = function(req, res) {
  var username = req.body.username;
  var password = req.body.password;

  var user = new Parse.User();
  user.set('username', username);
  user.set('password', password);

  user.signUp().then(function(user) {
    res.redirect('/');
  }, function(error) {
    res.render('user/signup', {
      title: 'Sign Up',
      errorFlash: error.message
    });
  });
};

exports.login = function(req, res) {
  if (Parse.User.current()) {
    res.redirect('/');
  } else {
    res.render('user/login', {
      title: 'Login' 
    });
  }
};

exports.doLogin = function(req, res) {
  Parse.User.logIn(req.body.username, req.body.password).then(function(user) {
    res.redirect('/');
  }, function(error) {
    // Show error message and let the user try again
    res.render('user/login', {
      title: 'Login',
      errorFlash: error.message
    });
  });
};

exports.doLogout = function(req, res) {
  Parse.User.logOut();
  res.redirect('/');
};
