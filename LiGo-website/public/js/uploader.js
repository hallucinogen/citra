Uploader = Backbone.View.extend({
  events: {
    'submit': 'upload',
    'click .upload': 'showFileExplorer',
    'change input[type=file]': 'previewFile'
  },

  supported: {
    filereader: typeof FileReader != 'undefined',
    dnd: 'draggable' in document.createElement('span'),
    formdata: !!window.FormData,
    progress: 'upload' in new XMLHttpRequest
  },

  acceptedTypes: {
    'image/png': true,
    'image/jpeg': true,
    'image/gif': true
  },

  imageConstructor: {
    'laptop-image': 'makeLaptopImage',
    'iphone-image': 'makePhoneImage',
    'tablet-image': 'makeTabletImage'
  },

  initialize: function() {
    if (this.supported.dnd) {
      this.events['dragenter .holder'] = 'addHover';
      this.events['dragleave .holder'] = 'removeHover';
      this.events['dragover .holder'] = 'preventDefault';
      this.events['drop .holder'] = 'dropFile'
    }
    this.mainImageInput = $('input[name=mainImage]')[0];
    this.phoneImageInput = $('input[name=phoneImage]')[0];
    this.tabletImageInput = $('input[name=tabletImage]')[0];

    this.delegateEvents();
  },

  dropFile: function(e) {
    var holder = $(e.currentTarget);
    var file = e.originalEvent.dataTransfer.files;
    var formData = this.supported.formdata ? new FormData() : null;
    holder.removeClass('well');

    if (!file.length) return;

    // preview
    file = file[0];
    //formdata.append('file', file);
    this.previewFileWithHolder(file, holder);

    // submit
    e.preventDefault();
  },

  previewFile: function(e) {
    var elem = $(e.currentTarget);
    var file = e.currentTarget.files;
    var holder = elem.parent().find('.holder').first();

    if (!file.length) return;

    this.previewFileWithHolder(file[0], holder);
  },

  previewFileWithHolder: function(file, holder) {
    if (this.supported.filereader && this.acceptedTypes[file.type]) {
      var reader = new FileReader();
      var self = this;
      reader.onload = function(e) {
        var image = new Image();
        var imageClass = holder.attr('data-image-class');
        image.src = e.target.result;
        image.setAttribute('class', imageClass);
        var containedImage = self[self.imageConstructor[imageClass]](image);

        holder.empty();
        holder.append(containedImage);

        if (holder.attr('id') === 'main-image-holder') {
          // TODO: optimize this by caching
          $('#phone-image-form').removeClass('hidden');
          $('#tablet-image-form').removeClass('hidden');
          var phoneHolder = $('#phone-image-form').find('.holder').first();
          var tabletHolder = $('#tablet-image-form').find('.holder').first();

          var phoneImage = new Image();
          phoneImage.src = e.target.result;
          phoneImage.setAttribute('class', 'iphone-image');
          var tabletImage = new Image();
          tabletImage.src = e.target.result;
          tabletImage.setAttribute('class', 'tablet-image');

          phoneHolder.empty();
          phoneHolder.append(self.makePhoneImage(phoneImage));
          tabletHolder.empty();
          tabletHolder.append(self.makeTabletImage(tabletImage));
        }
      };

      reader.readAsDataURL(file);
    }
  },

  makeLaptopImage: function(image) {
    var container = $("<div class='laptop-internal'></div>").append(image);
    return $("\
      <div class='image'>\
        <img class='img-responsive laptop-container' src='/images/laptop_placeholder.png'>\
      </div>\
    ").append(container);
  },

  makePhoneImage: function(image) {
    var androidImage = new Image();
    androidImage.src = image.src;
    androidImage.setAttribute('class', 'android-image');
    var iPhoneContainer = $("<div class='iphone-internal'></div>").append(image);
    var containerI = $("\
      <div class='col-md-6'>\
        <img class='img-responsive iphone-container' src='/images/iphone_placeholder.png'>\
      </div>\
    ").append(iPhoneContainer);
    var androidContainer = $("<div class='android-internal'></div>").append(androidImage);
    var containerA = $("\
      <div class='col-md-6'>\
        <img class='img-responsive android-container' src='/images/android_placeholder.png'>\
      </div>\
    ").append(androidContainer);

    return $("<div class='image'></div>").append(containerI).append(containerA);
  },

  makeTabletImage: function(image) {
    var container = $("<div class='tablet-internal'></div>").append(image);
    return $("\
      <div class='image'>\
        <img class='img-responsive tablet-container' src='/images/tablet_placeholder.png'>\
      </div>\
    ").append(container);
  },

  addHover: function(e) {
    var elem = $(e.currentTarget);
    elem.addClass('well');

    return false;
  },

  removeHover: function(e) {
    var elem = $(e.currentTarget);
    elem.removeClass('well');

    return false;
  },

  preventDefault: function(e) {
    return false;
  },

  showFileExplorer: function(e) {
    var elem = $(e.currentTarget);
    elem.parent().find('input[type=file]').first().click();
    return false;
  },

  upload: function(e) {
    var self = this;
    // preventDefault here to prevent something goes wrong and we can't intercept the event later
    e.preventDefault();

    if (this.mainImageInput.files.length > 0) {
      this.$("button[type=submit]").html("Uploading <img src='/images/spinner.gif' />");
      var mainImageFile = this.mainImageInput.files[0];
      var phoneImageFile = this.phoneImageInput.files[0];
      var tabletImageFile = this.tabletImageInput.files[0];
      var name = "image.jpg";
      var mainImage = new Parse.File(name, mainImageFile);
      var phoneImage = null;
      var tabletImage = null;
      if (phoneImageFile) {
        phoneImage = new Parse.File(name, phoneImageFile);
      }
      if (tabletImageFile) {
        tabletImage = new Parse.File(name, tabletImageFile);
      }

      var formJSON = {};
      _.each(this.$el.serializeArray(), function(elem) {
        formJSON[elem.name] = elem.value;
      });

      // First, we save the file using the javascript sdk
      mainImage.save().then(function() {
        formJSON.mainFile = {
          "__type": "File",
          "url": mainImage.url(),
          "name": mainImage.name()
        };
        if (phoneImage) {
          return phoneImage.save();
        } else {
          return Parse.Promise.as();
        }
      }).then(function() {
        if (phoneImage) {
          formJSON.phoneFile = {
            "__type": "File",
            "url": phoneImage.url(),
            "name": phoneImage.name()
          };
        }
        if (tabletImage) {
          return tabletImage.save();
        } else {
          return Parse.Promise.as();
        }
      }).then(function() {
        if (tabletImage) {
          formJSON.tabletFile = {
            "__type": "File",
            "url": tabletImage.url(),
            "name": tabletImage.name()
          };
        }
        console.log(formJSON);
        $.post("/wallpaper", formJSON, function(data) {
          window.location.href = "/wallpaper/" + data.id;
        });
      });
    } else {
      // TODO: flash error
    }

    return false;
  }
});


$(function() {
  // Make all of special links magically post the form
  // when it has a particular data-action associated
  $("a[data-action='post']").click(function(e) {
    var el = $(e.target);
    el.closest("form").submit();
    return false;
  });
});
